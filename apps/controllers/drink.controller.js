const drinkModel = require("../models/drink.model");
const mongoose = require("mongoose");

const createDrink = async (req, res) => {
    // B1 - collect data
    const { maNuocUong, tenNuocUong, donGia } = req.body;

    // B2 - validate
    if (!maNuocUong) {
        return res.status(400).json({
            message: "maNuocUong khong hop le",
        });
    }
    if (!tenNuocUong) {
        return res.status(400).json({
            message: "tenNuocUong khong hop le",
        });
    }
    if (!donGia || donGia < 0) {
        return res.status(400).json({
            message: "donGia khong hop le",
        });
    }

    try {
        // B3 - save to database
        let newDrink = {
            maNuocUong,
            tenNuocUong,
            donGia,
        };

        const result = await drinkModel.create(newDrink);
        return res.status(201).json({ message: "Successful !", data: result });
    } catch (error) {
        return res.status(500).json({
            message: "Co loi xay ra",
        });
    }
};

const getAllDrink = async (req, res) => {
    try {
        const result = await drinkModel.find();
        return res.status(200).json({ message: "Successful !", data: result });
    } catch (error) {
        return res.status(500).json({
            message: "Co loi xay ra",
        });
    }
};

const getDrinkById = async (req, res) => {
    // B1 - Collect data
    const _id = req.params.id;
    if (!mongoose.Types.ObjectId.isValid(_id)) {
        return res.status(400).json({
            message: "Id khong hop le",
        });
    }

    try {
        const result = await drinkModel.findById(_id);
        if (result) {
            return res
                .status(200)
                .json({ message: "Successful !", data: result });
        } else {
            return res.status(404).json({
                message: "Khong tim thay thong tin nuoc uong ",
            });
        }
    } catch (error) {
        return res.status(500).json({
            message: "Co loi xay ra",
        });
    }
};

const updateDrinkById = async (req, res) => {
    // B1 - Collect data
    const _id = req.params.id;
    if (!mongoose.Types.ObjectId.isValid(_id)) {
        return res.status(400).json({
            message: "Id khong hop le",
        });
    }
    const { maNuocUong, tenNuocUong, donGia } = req.body;

    if (maNuocUong === "") {
        return res.status(400).json({
            message: "maNuocUong khong hop le",
        });
    }
    if (tenNuocUong === "") {
        return res.status(400).json({
            message: "tenNuocUong khong hop le",
        });
    }
    if (donGia < 0) {
        return res.status(400).json({
            message: "donGia khong hop le",
        });
    }

    try {
        let newUpdateDrink = {};
        if (maNuocUong) {
            newUpdateDrink.maNuocUong = maNuocUong;
        }
        if (tenNuocUong) {
            newUpdateDrink.tenNuocUong = tenNuocUong;
        }
        if (donGia) {
            newUpdateDrink.donGia = donGia;
        }
        const result = await drinkModel.findByIdAndUpdate(_id, newUpdateDrink);
        if (result) {
            return res
                .status(200)
                .json({ message: "Successful !", data: result });
        } else {
            return res.status(404).json({
                message: "Khong tim thay thong tin Drink ",
            });
        }
    } catch (error) {
        return res.status(500).json({
            message: "Co loi xay ra",
        });
    }
};

const deleteDrinkById = async (req, res) => {
    // B1 - Collect data
    const _id = req.params.id;
    if (!mongoose.Types.ObjectId.isValid(_id)) {
        return res.status(400).json({
            message: "Id khong hop le",
        });
    }
    try {
        const result = await drinkModel.findByIdAndRemove(_id);
        if (result) {
            return res.status(200).json({ message: "Successful !" });
        } else {
            return res.status(404).json({
                message: "Khong tim thay thong tin nuoc uong ",
            });
        }
    } catch (error) {
        return res.status(500).json({
            message: "Co loi xay ra",
        });
    }
};

module.exports = {
    createDrink,
    getAllDrink,
    getDrinkById,
    deleteDrinkById,
    updateDrinkById,
};
