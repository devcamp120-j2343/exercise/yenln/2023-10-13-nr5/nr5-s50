const orderModel = require("../models/order.model");
const userModel = require("../models/user.model");
const mongoose = require("mongoose");

const createOrder2 = async (req, res) => {
    // B1 - collect data
    const requestOrder = req.body;
    console.log("::::requestOrder", requestOrder);
    try {
        const order = await orderModel.create({
            pizzaSize: requestOrder.kichCo,
            pizzaType: requestOrder.loaiPizza,
            voucher: requestOrder.idVourcher,
            drink: requestOrder.idLoaiNuocUong,
            status: "open",
        });
        if (!order)
            return res.status(404).json({ message: "Create order fail !" });

        console.log(":::::::::", order);
        const user = await userModel.findOne({ email: requestOrder.email });
        console.log("::::::::: user", user);
        if (user.length === 0) {
            const newUser = await userModel.create({
                fullName: requestOrder.hoTen,
                email: requestOrder.email,
                address: requestOrder.diaChi,
                phone: requestOrder.soDienThoai,
                orders: [order._id],
            });
            console.log("newUser:::::", newUser);
            if (!newUser)
                return res.status(404).json({ message: "Create fail !" });
        } else {
            console.log("user._id:", user._id);
            const updateUser = await userModel.findByIdAndUpdate(
                user._id,
                {
                    $push: { orders: order._id },
                },
                { new: true }
            );
            if (!updateUser)
                return res.status(404).json({ message: "Fail, try again !" });
        }
        return res.status(200).json(order);
    } catch (error) {
        console.log(error);
        return res.status(500).json({
            message: "Co loi xay ra",
        });
    }
};

const createOrder = async (req, res) => {
    // B1 - collect data
    const { pizzaSize, pizzaType, voucher, drink, status, _userId } = req.body;

    // B2 - validate
    if (!mongoose.Types.ObjectId.isValid(_userId)) {
        return res.status(400).json({
            message: "user id khong hop le",
        });
    }
    if (!pizzaSize) {
        return res.status(400).json({
            message: "pizzaSize khong hop le",
        });
    }
    if (!pizzaType) {
        return res.status(400).json({
            message: "pizzaType khong hop le",
        });
    }
    if (!mongoose.Types.ObjectId.isValid(voucher)) {
        return res.status(400).json({
            message: "voucher khong hop le",
        });
    }
    if (!mongoose.Types.ObjectId.isValid(drink)) {
        return res.status(400).json({
            message: "drink khong hop le",
        });
    }
    if (!status) {
        return res.status(400).json({
            message: "status khong hop le",
        });
    }

    try {
        // B3 - save to database
        const result = await orderModel.create({
            pizzaSize,
            pizzaType,
            voucher,
            drink,
            status,
        });
        const updateUser = await userModel.findByIdAndUpdate(_userId, {
            $push: { orders: result._id },
        });
        if (!updateUser)
            return res.status(404).json({
                message: "Co loi xay ra",
            });

        return res
            .status(201)
            .json({ message: "Create successful !", data: result });
    } catch (error) {
        console.log(error);
        return res.status(500).json({
            message: "Co loi xay ra",
        });
    }
};

const getAllOrder = async (req, res) => {
    try {
        const result = await orderModel.find();
        return res.status(200).json({ message: "Successful !", data: result });
    } catch (error) {
        return res.status(500).json({
            message: "Co loi xay ra",
        });
    }
};

const getOrderById = async (req, res) => {
    // B1 - Collect data
    const _id = req.params.id;
    if (!mongoose.Types.ObjectId.isValid(_id)) {
        return res.status(400).json({
            message: "Id khong hop le",
        });
    }

    try {
        const result = await orderModel.findById(_id);
        if (result) {
            return res
                .status(200)
                .json({ message: "Successful !", data: result });
        } else {
            return res.status(404).json({
                message: "Khong tim thay thong tin Order ",
            });
        }
    } catch (error) {
        return res.status(500).json({
            message: "Co loi xay ra",
        });
    }
};

const updateOrderById = async (req, res) => {
    // B1 - Collect data
    const _id = req.params.id;
    if (!mongoose.Types.ObjectId.isValid(_id)) {
        return res.status(400).json({
            message: "Id khong hop le",
        });
    }
    const { pizzaSize, pizzaType, voucher, drink, status } = req.body;

    if (pizzaSize === "") {
        return res.status(400).json({
            message: "pizzaSize khong hop le",
        });
    }
    if (pizzaType === "") {
        return res.status(400).json({
            message: "pizzaType khong hop le",
        });
    }
    if (voucher === "") {
        return res.status(400).json({
            message: "voucher khong hop le",
        });
    }
    if (drink === "") {
        return res.status(400).json({
            message: "drink khong hop le",
        });
    }
    if (status === "") {
        return res.status(400).json({
            message: "status khong hop le",
        });
    }

    try {
        let newUpdateOrder = {};
        if (pizzaSize) {
            newUpdateOrder.pizzaSize = pizzaSize;
        }
        if (pizzaType) {
            newUpdateOrder.pizzaType = pizzaType;
        }
        if (voucher) {
            newUpdateOrder.voucher = voucher;
        }
        if (drink) {
            newUpdateOrder.drink = drink;
        }
        if (status) {
            newUpdateOrder.status = status;
        }

        const result = await orderModel.findByIdAndUpdate(_id, newUpdateOrder);
        if (result) {
            return res
                .status(200)
                .json({ message: "Update successful !", data: result });
        } else {
            return res.status(404).json({
                message: "Khong tim thay thong tin Order ",
            });
        }
    } catch (error) {
        return res.status(500).json({
            message: "Co loi xay ra",
        });
    }
};

const deleteOrderById = async (req, res) => {
    // B1 - Collect data
    const _id = req.params.id;
    if (!mongoose.Types.ObjectId.isValid(_id)) {
        return res.status(400).json({
            message: "Id khong hop le",
        });
    }
    try {
        const result = await orderModel.findByIdAndRemove(_id);
        if (result) {
            return res.status(200).json({ message: "Delete successful !" });
        } else {
            return res.status(404).json({
                message: "Khong tim thay thong tin Order ",
            });
        }
    } catch (error) {
        return res.status(500).json({
            message: "Co loi xay ra",
        });
    }
};

module.exports = {
    createOrder,
    getAllOrder,
    getOrderById,
    deleteOrderById,
    updateOrderById,
    createOrder2,
};
