const userModel = require("../models/user.model");
const mongoose = require("mongoose");

const getUserWithLimitSkip = async (req, res) => {
    let skip = req.query.skip;
    let limit = req.query.limit;

    let query = userModel.find().sort({ fullName: "desc" });
    if (skip) {
        query.skip(1);
    }
    if (limit) {
        query.limit(limit);
    }

    try {
        const result = await query.exec();

        return res.status(200).json({ message: "Successful !", data: result });
    } catch (error) {
        return res.status(500).json({
            message: "Co loi xay ra",
        });
    }
};

const getAllUserWithSort = async (req, res) => {
    try {
        const result = await userModel.find().sort({ fullName: "desc" }).exec();

        return res.status(200).json({ message: "Successful !", data: result });
    } catch (error) {
        return res.status(500).json({
            message: "Co loi xay ra",
        });
    }
};

const getAllUserWithSkip = async (req, res) => {
    let skip = req.query.skip;

    let query = userModel.find();
    if (skip) {
        query.skip(1);
    }

    try {
        const result = await query.exec();

        return res.status(200).json({ message: "Successful !", data: result });
    } catch (error) {
        return res.status(500).json({
            message: "Co loi xay ra",
        });
    }
};

const getAllUserWithLimit = async (req, res) => {
    let limit = req.query.limit;

    let query = userModel.find();
    if (limit) {
        query.limit(limit);
    }

    try {
        const result = await query.exec();

        return res.status(200).json({ message: "Successful !", data: result });
    } catch (error) {
        return res.status(500).json({
            message: "Co loi xay ra",
        });
    }
};

const createUser = async (req, res) => {
    // B1 - collect data
    const { fullName, email, address, phone } = req.body;

    // B2 - validate
    if (!fullName) {
        return res.status(400).json({
            message: "fullName khong hop le",
        });
    }
    if (!email) {
        return res.status(400).json({
            message: "fullName khong hop le",
        });
    }
    if (!address) {
        return res.status(400).json({
            message: "fullName khong hop le",
        });
    }
    if (!phone) {
        return res.status(400).json({
            message: "fullName khong hop le",
        });
    }

    try {
        // B3 - save to database
        let newUser = { fullName, email, address, phone };
        const result = await userModel.create(newUser);

        return res
            .status(201)
            .json({ message: "Create successful !", data: result });
    } catch (error) {
        console.log(error);
        return res.status(500).json({
            message: "Co loi xay ra",
        });
    }
};

const getAllUser = async (req, res) => {
    try {
        const result = await userModel.find();
        return res.status(200).json({ message: "Successful !", data: result });
    } catch (error) {
        return res.status(500).json({
            message: "Co loi xay ra",
        });
    }
};

const getUserById = async (req, res) => {
    // B1 - Collect data
    const _id = req.params.id;
    if (!mongoose.Types.ObjectId.isValid(_id)) {
        return res.status(400).json({
            message: "Id khong hop le",
        });
    }

    try {
        const result = await userModel.findById(_id);
        if (result) {
            return res
                .status(200)
                .json({ message: "Successful !", data: result });
        } else {
            return res.status(404).json({
                message: "Khong tim thay thong tin User ",
            });
        }
    } catch (error) {
        return res.status(500).json({
            message: "Co loi xay ra",
        });
    }
};

const updateUserById = async (req, res) => {
    // B1 - Collect data
    const _id = req.params.id;
    if (!mongoose.Types.ObjectId.isValid(_id)) {
        return res.status(400).json({
            message: "Id khong hop le",
        });
    }
    const { fullName, email, address, phone } = req.body;

    if (fullName === "") {
        return res.status(400).json({
            message: "fullName khong hop le",
        });
    }
    if (email === "") {
        return res.status(400).json({
            message: "email khong hop le",
        });
    }
    if (address === "") {
        return res.status(400).json({
            message: "address khong hop le",
        });
    }
    if (phone === "") {
        return res.status(400).json({
            message: "phone khong hop le",
        });
    }

    try {
        let newUpdateUser = {};
        if (fullName) {
            newUpdateUser.fullName = fullName;
        }
        if (email) {
            newUpdateUser.email = email;
        }
        if (address) {
            newUpdateUser.address = address;
        }
        if (phone) {
            newUpdateUser.phone = phone;
        }
        const result = await userModel.findByIdAndUpdate(_id, newUpdateUser);
        if (result) {
            return res
                .status(200)
                .json({ message: "Update successful !", data: result });
        } else {
            return res.status(404).json({
                message: "Khong tim thay thong tin User ",
            });
        }
    } catch (error) {
        return res.status(500).json({
            message: "Co loi xay ra",
        });
    }
};

const deleteUserById = async (req, res) => {
    // B1 - Collect data
    const _id = req.params.id;
    if (!mongoose.Types.ObjectId.isValid(_id)) {
        return res.status(400).json({
            message: "Id khong hop le",
        });
    }
    try {
        const result = await userModel.findByIdAndRemove(_id);
        if (result) {
            return res.status(200).json({ message: "Delete successful !" });
        } else {
            return res.status(404).json({
                message: "Khong tim thay thong tin User ",
            });
        }
    } catch (error) {
        return res.status(500).json({
            message: "Co loi xay ra",
        });
    }
};

module.exports = {
    createUser,
    getAllUser,
    getUserById,
    deleteUserById,
    updateUserById,
    getAllUserWithLimit,
    getAllUserWithSkip,
    getAllUserWithSort,
    getUserWithLimitSkip,
};
