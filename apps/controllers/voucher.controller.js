const voucherModel = require("../models/voucher.model");
const mongoose = require("mongoose");

const createVoucher = async (req, res) => {
    // B1 - collect data
    const { maVoucher, phanTramGiamGia, ghiChu } = req.body;

    // B2 - validate
    if (!maVoucher) {
        return res.status(400).json({
            message: "maVoucher khong hop le",
        });
    }
    if (!phanTramGiamGia || phanTramGiamGia < 0) {
        return res.status(400).json({
            message: "phanTramGiamGia khong hop le",
        });
    }

    try {
        // B3 - save to database
        let newVoucher = {
            maVoucher,
            phanTramGiamGia,
            ghiChu,
        };

        const result = await voucherModel.create(newVoucher);
        return res.status(201).json({ message: "Successful !", data: result });
    } catch (error) {
        return res.status(500).json({
            message: "Co loi xay ra",
        });
    }
};

const getAllVoucher = async (req, res) => {
    try {
        const result = await voucherModel.find();
        return res.status(200).json({ message: "Successful !", data: result });
    } catch (error) {
        return res.status(500).json({
            message: "Co loi xay ra",
        });
    }
};

const getVoucherById = async (req, res) => {
    // B1 - Collect data
    const _id = req.params.id;
    if (!mongoose.Types.ObjectId.isValid(_id)) {
        return res.status(400).json({
            message: "Id khong hop le",
        });
    }

    try {
        const result = await voucherModel.findById(_id);
        if (result) {
            return res
                .status(200)
                .json({ message: "Successful !", data: result });
        } else {
            return res.status(404).json({
                message: "Khong tim thay thong tin voucher ",
            });
        }
    } catch (error) {
        return res.status(500).json({
            message: "Co loi xay ra",
        });
    }
};

const updateVoucherById = async (req, res) => {
    // B1 - Collect data
    const _id = req.params.id;
    if (!mongoose.Types.ObjectId.isValid(_id)) {
        return res.status(400).json({
            message: "Id khong hop le",
        });
    }
    const { maVoucher, phanTramGiamGia, ghiChu } = req.body;

    if (maVoucher === "") {
        return res.status(400).json({
            message: "maNuocUong khong hop le",
        });
    }
    if (phanTramGiamGia < 0) {
        return res.status(400).json({
            message: "donGia khong hop le",
        });
    }

    try {
        let newUpdateVoucher = {};
        if (maVoucher) {
            newUpdateVoucher.maVoucher = maVoucher;
        }
        if (phanTramGiamGia) {
            newUpdateVoucher.phanTramGiamGia = phanTramGiamGia;
        }
        if (ghiChu) {
            newUpdateVoucher.ghiChu = ghiChu;
        }
        const result = await voucherModel.findByIdAndUpdate(
            _id,
            newUpdateVoucher
        );
        if (result) {
            return res
                .status(200)
                .json({ message: "Successful !", data: result });
        } else {
            return res.status(404).json({
                message: "Khong tim thay thong tin Voucher ",
            });
        }
    } catch (error) {
        return res.status(500).json({
            message: "Co loi xay ra",
        });
    }
};

const deleteVoucherById = async (req, res) => {
    // B1 - Collect data
    const _id = req.params.id;
    if (!mongoose.Types.ObjectId.isValid(_id)) {
        return res.status(400).json({
            message: "Id khong hop le",
        });
    }
    try {
        const result = await voucherModel.findByIdAndRemove(_id);
        if (result) {
            return res.status(200).json({ message: "Successful !" });
        } else {
            return res.status(404).json({
                message: "Khong tim thay thong tin voucher ",
            });
        }
    } catch (error) {
        return res.status(500).json({
            message: "Co loi xay ra",
        });
    }
};

module.exports = {
    createVoucher,
    getAllVoucher,
    getVoucherById,
    deleteVoucherById,
    updateVoucherById,
};
