const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const orderSchema = new Schema(
    {
        orderCode: {
            type: String,
            unique: true,
            default: () => Math.floor(Math.random() * 10000),
        },
        pizzaSize: {
            type: String,
            required: true,
        },
        pizzaType: {
            type: String,
            required: true,
        },
        voucher: {
            type: mongoose.Types.ObjectId,
            ref: "Voucher",
        },
        drink: {
            type: mongoose.Types.ObjectId,
            ref: "Drink",
        },
        status: {
            type: String,
            required: true,
        },
    },
    {
        timestamps: true,
    }
);

module.exports = mongoose.model("Order", orderSchema);
