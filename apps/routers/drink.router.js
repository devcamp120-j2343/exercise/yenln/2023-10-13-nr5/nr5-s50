const express = require("express");
const router = express.Router();

const {
    createDrink,
    getAllDrink,
    getDrinkById,
    deleteDrinkById,
    updateDrinkById,
} = require("../controllers/drink.controller");

router.get("/", getAllDrink);

router.get("/:id", getDrinkById);

router.post("/", createDrink);

router.put("/:id", updateDrinkById);

router.delete("/:id", deleteDrinkById);

module.exports = router;
