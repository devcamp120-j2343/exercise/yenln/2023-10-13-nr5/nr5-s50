const express = require("express");
const router = express.Router();

const {
    createOrder,
    getAllOrder,
    getOrderById,
    deleteOrderById,
    updateOrderById,
} = require("../controllers/order.controller");

router.get("/", getAllOrder);

router.get("/:id", getOrderById);

router.post("/", createOrder);

router.put("/:id", updateOrderById);

router.delete("/:id", deleteOrderById);

module.exports = router;
