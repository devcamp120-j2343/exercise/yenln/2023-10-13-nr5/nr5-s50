const express = require("express");
const router = express.Router();

const {
    createUser,
    getAllUser,
    getUserById,
    deleteUserById,
    updateUserById,
    getAllLimitUser,
} = require("../controllers/user.controller");

router.get("/", getAllUser);

router.get("/:id", getUserById);

router.post("/", createUser);

router.put("/:id", updateUserById);

router.delete("/:id", deleteUserById);

module.exports = router;
