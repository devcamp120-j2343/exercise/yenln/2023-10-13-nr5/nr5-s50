const express = require("express");
const app = express();
const path = require("path");

const mongoose = require("mongoose");
const drinkSchema = require("./apps/models/drink.model");
const voucherSchema = require("./apps/models/voucher.model");
const orderSchema = require("./apps/models/order.model");
const userSchema = require("./apps/models/user.model");

const drinkRouter = require("./apps/routers/drink.router");
const voucherRouter = require("./apps/routers/voucher.router");
const userRouter = require("./apps/routers/user.router");
const orderRouter = require("./apps/routers/order.router");
const {
    getAllUserWithLimit,
    getAllUserWithSkip,
    getAllUserWithSort,
    getUserWithLimitSkip,
} = require("./apps/controllers/user.controller");
const { createOrder2 } = require("./apps/controllers/order.controller");
const { getAllDrink } = require("./apps/controllers/drink.controller");

app.use(express.json());
app.use(express.static(__dirname + "/views"));

// kết nối mongodb
const connectString = "mongodb://localhost:27017/CRUD_Pizza365";
mongoose
    .connect(connectString)
    .then((_) => {
        console.log("Connect mongodb successfully !");
    })
    .catch((_) => {
        console.log("Connect mongodb failed !!!");
    });

app.get("/", (req, res) => {
    console.log(__dirname);

    return res.sendFile(path.join(__dirname, "/views/index.html"));
});

app.use("/api/v1/drink/", drinkRouter);
app.use("/api/v1/voucher/", voucherRouter);
app.use("/api/v1/user/", userRouter);
app.use("/api/v1/order/", orderRouter);

app.get("/limit-users", getAllUserWithLimit);
app.get("/skip-users", getAllUserWithSkip);
app.get("/sort-users", getAllUserWithSort);
app.get("/skip-limit-users", getUserWithLimitSkip);
app.get("/sort-skip-limit-users", getUserWithLimitSkip);

app.post("/devcamp-pizza365/orders", createOrder2);
app.post("/devcamp-pizza365/drinks", getAllDrink);

const PORT = 8000;
app.listen(PORT, () => {
    console.log(`App listening on ${PORT}`);
});
